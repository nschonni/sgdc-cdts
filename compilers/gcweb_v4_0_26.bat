SET version=v4_0_26
java -jar SoyToJsSrcCompiler.jar --outputPathFormat nomin.js --srcs ..\app\cls\WET\gcweb\%version%\cdts\uncompiled\wet-en.soy,..\app\cls\WET\gcweb\%version%\cdts\uncompiled\serverPage-en.soy,..\app\cls\WET\gcweb\%version%\cdts\uncompiled\appPage-en.soy
java -jar minify.jar --js nomin.js --js_output_file ..\app\cls\WET\gcweb\%version%\cdts\compiled\wet-en.js
java -jar SoyToJsSrcCompiler.jar --outputPathFormat nomin.js --srcs ..\app\cls\WET\gcweb\%version%\cdts\uncompiled\wet-fr.soy,..\app\cls\WET\gcweb\%version%\cdts\uncompiled\serverPage-fr.soy,..\app\cls\WET\gcweb\%version%\cdts\uncompiled\appPage-fr.soy
java -jar minify.jar --js nomin.js --js_output_file ..\app\cls\WET\gcweb\%version%\cdts\compiled\wet-fr.js
pause