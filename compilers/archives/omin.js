// This file was automatically generated from wet-en.soy.
// Please don't edit this file by hand.

if (typeof wet == 'undefined') { var wet = {}; }
if (typeof wet.builder == 'undefined') { wet.builder = {}; }


wet.builder.refTop = function(opt_data, opt_ignored) {
  opt_data = opt_data || {};
  return '<!--[if gte IE 9 | !IE ]><!--><link href="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/assets/favicon.ico" rel="icon" type="image/x-icon"><link rel="stylesheet" href="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/css/wet-boew.min.css"><!--<![endif]--><link rel="stylesheet" href="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/css/theme.min.css"><!--[if lt IE 9]><link href="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/assets/favicon.ico" rel="shortcut icon" /><link rel="stylesheet" href="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/css/ie8-wet-boew.min.css" /><link rel="stylesheet" href="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/css/ie8-theme.min.css" /><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"><\/script><script src="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/js/ie8-wet-boew.min.js"><\/script><![endif]-->';
};


wet.builder.top = function(opt_data, opt_ignored) {
  opt_data = opt_data || {};
  var output = '<ul id="wb-tphp"><li class="wb-slc"><a class="wb-sl" href="#wb-cont">Skip to main content</a></li><li class="wb-slc visible-sm visible-md visible-lg"><a class="wb-sl" href="#wb-info">Skip to "About this site"</a></li></ul><header role="banner"><div id="wb-bnr"><div id="wb-bar"><div class="container"><div class="row"><object id="gcwu-sig" type="image/svg+xml" tabindex="-1" role="img" data="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/assets/sig-en.svg" aria-label="Government of Canada"></object><ul id="gc-bar" class="list-inline"><li><a href="http://www.canada.ca/en/index.html" rel="external">Canada.ca</a></li><li><a href="http://www.canada.ca/en/services/index.html" rel="external">Services</a></li><li><a href="http://www.canada.ca/en/gov/dept/index.html" rel="external">Departments</a></li>';
  if (opt_data.lngLinks != null) {
    output += '<li id="wb-lng"><h2>Language selection</h2><ul class="list-inline">';
    var linkList89 = opt_data.lngLinks;
    var linkListLen89 = linkList89.length;
    for (var linkIndex89 = 0; linkIndex89 < linkListLen89; linkIndex89++) {
      var linkData89 = linkList89[linkIndex89];
      output += '<li><a lang="' + soy.$$escapeHtml(linkData89.lang) + '" href="' + soy.$$escapeHtml(linkData89.href) + '">' + soy.$$escapeHtml(linkData89.text) + '</a></li>';
    }
    output += '</ul></li>';
  }
  output += '</ul><section class="wb-mb-links col-xs-12 visible-sm visible-xs" id="wb-glb-mn"><h2>Search and menus</h2><ul class="pnl-btn list-inline text-right"><li><a href="#mb-pnl" title="Search and menus" aria-controls="mb-pnl" class="overlay-lnk btn btn-sm btn-default" role="button"><span class="glyphicon glyphicon-search"><span class="glyphicon glyphicon-th-list"><span class="wb-inv">Search and menus</span></span></span></a></li></ul><div id="mb-pnl"></div></section></div></div></div><div class="container"><div class="row"><div id="wb-sttl" class="col-md-5">' + ((opt_data.siteTitle != null) ? '<a href="' + soy.$$escapeHtml(opt_data.siteTitle.href) + '"><span>' + soy.$$escapeHtml(opt_data.siteTitle.text) + '</span></a>' : '<a href="http://www.esdc.gc.ca/eng/home.shtml"><span>Employment and Social Development Canada</span></a>') + '</div><object id="wmms" type="image/svg+xml" tabindex="-1" role="img" data="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/assets/wmms.svg" aria-label="Symbol of the Government of Canada"></object>' + ((opt_data.search != false) ? '<section id="wb-srch" class="visible-md visible-lg"><h2>Search</h2><form action="http://search.gc.ca/rGs/s_r?langs=eng&#38;st1rt=0&#38;num=10&#38;cdn=hrsdc&#38;st=s" method="get" role="search" class="form-inline"><div class="form-group"><label for="wb-srch-q">Search website</label><input id="wb-srch-q" class="form-control" name="q" type="search" value="" size="27" maxlength="150"><input type="hidden" name="st" value="s" /><input type="hidden" name="s5bm3ts21rch" value="x" /><input type="hidden" name="num" value="10" /><input name="output" value="xml_no_dtd" type="hidden" /><input name="proxystylesheet" value="hrsdc_wet_r12" type="hidden" /><input type="hidden" name="st1rt" value="0" /><input type="hidden" name="langs" value="eng" /><input type="hidden" name="cdn" value="hrsdc" /></div><button type="submit" id="wb-srch-sub" class="btn btn-default">Search</button></form></section>' : '') + '</div></div></div>' + ((opt_data.siteMenu != false) ? '<nav role="navigation" id="wb-sm" data-ajax-replace="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/ajax/sitemenu-en.html" data-trgt="mb-pnl" class="wb-menu visible-md visible-lg" typeof="SiteNavigationElement"><div class="container nvbar"><h2>Topics menu</h2><div class="row"><ul class="list-inline menu"><li><a href="http://wet-boew.github.io/wet-boew/index-en.html">WET project</a></li><li><a href="http://wet-boew.github.io/wet-boew/docs/start-en.html#implement">Implement WET</a></li><li><a href="http://wet-boew.github.io/wet-boew/docs/start-en.html">Contribute to WET</a></li></ul></div></div></nav>' : '');
  if (opt_data.breadcrumbs != false) {
    if (opt_data.breadcrumbs != null) {
      output += '<nav role="navigation" id="wb-bc" property="breadcrumb"><h2>You are here:</h2><div class="container"><div class="row"><ol class="breadcrumb">';
      var itemList142 = opt_data.breadcrumbs;
      var itemListLen142 = itemList142.length;
      for (var itemIndex142 = 0; itemIndex142 < itemListLen142; itemIndex142++) {
        var itemData142 = itemList142[itemIndex142];
        output += '<li>' + ((itemData142.acronym != null) ? '<abbr title="' + soy.$$escapeHtml(itemData142.acronym) + '">' : '') + ((itemData142.href != null) ? '<a href="' + soy.$$escapeHtml(itemData142.href) + '">' : '') + soy.$$escapeHtml(itemData142.title) + ((itemData142.href != null) ? '</a>' : '') + ((itemData142.acronym != null) ? '</abbr>' : '') + '</li>';
      }
      output += '</ol></div></div></nav>';
    } else {
      output += '<nav role="navigation" id="wb-bc" property="breadcrumb"><h2>You are here:</h2><div class="container"><div class="row"><ol class="breadcrumb"><li><a href="http://www.esdc.gc.ca/eng/home.shtml">Home</a></li></ol></div></div></nav>';
    }
  }
  output += '</header>';
  return output;
};


wet.builder.preFooter = function(opt_data, opt_ignored) {
  return '<dl id="wb-dtmd"><dt>Date modified:&#32;</dt><dd><time property="dateModified">' + soy.$$escapeHtml(opt_data.dateModified) + '</time></dd></dl>';
};


wet.builder.footer = function(opt_data, opt_ignored) {
  opt_data = opt_data || {};
  var output = '<footer role="contentinfo" id="wb-info" class="visible-sm visible-md visible-lg wb-navcurr"><div class="container"><nav role="navigation"><h2>About this site</h2><ul id="gc-tctr" class="list-inline"><li><a rel="license" href="http://www.esdc.gc.ca/eng/terms/index.shtml">Terms and conditions</a></li><li><a href="http://www.esdc.gc.ca/eng/transparency/index.shtml">Transparency</a></li></ul><div class="row"><section class="col-sm-3"><h3>Contact us</h3><ul class="list-unstyled">';
  if (opt_data.contactLinks != null) {
    var linkList175 = opt_data.contactLinks;
    var linkListLen175 = linkList175.length;
    if (linkListLen175 > 0) {
      for (var linkIndex175 = 0; linkIndex175 < linkListLen175; linkIndex175++) {
        var linkData175 = linkList175[linkIndex175];
        output += '<li><a href="' + soy.$$escapeHtml(linkData175.href) + '">' + soy.$$escapeHtml(linkData175.text) + '</a></li>';
      }
    } else {
      output += '&nsbp;';
    }
  } else {
    output += '<li><a rel="external" href="http://www.servicecanada.gc.ca/eng/common/contactus/index.shtml">Service Enquiries</a></li><li><a rel="external" href="http://sage-geds.gc.ca/cgi-bin/direct500/eng/TE?FN=index.html">Employee Directory</a></li>';
  }
  output += '</ul></section><section class="col-sm-3"><h3>About</h3><ul class="list-unstyled">';
  if (opt_data.aboutLinks != null) {
    var linkList189 = opt_data.aboutLinks;
    var linkListLen189 = linkList189.length;
    if (linkListLen189 > 0) {
      for (var linkIndex189 = 0; linkIndex189 < linkListLen189; linkIndex189++) {
        var linkData189 = linkList189[linkIndex189];
        output += '<li><a href="' + soy.$$escapeHtml(linkData189.href) + '">' + soy.$$escapeHtml(linkData189.text) + '</a></li>';
      }
    } else {
      output += '&nsbp;';
    }
  } else {
    output += '<li><a href="http://www.esdc.gc.ca/eng/about/officials/index.shtml">Ministers and Officials</a></li><li><a href="http://www.esdc.gc.ca/eng/publications/index.shtml">Publications and Reports</a></li><li><a href="http://www.esdc.gc.ca/eng/acts/index.shtml">Acts and Regulations</a></li><li><a href="http://www.esdc.gc.ca/en/esdc/programs.page">Programs &#38; Services</a></li><li><a rel="external" href="http://www.labour.gc.ca/eng/home.shtml">Labour Program</a></li><li><a rel="external" href="http://www.seniors.gc.ca/eng/index.shtml">Canada.ca/Seniors</a></li>';
  }
  output += '</ul></section><section class="col-sm-3"><h3>News</h3><ul class="list-unstyled">';
  if (opt_data.newsLinks != null) {
    var linkList203 = opt_data.newsLinks;
    var linkListLen203 = linkList203.length;
    if (linkListLen203 > 0) {
      for (var linkIndex203 = 0; linkIndex203 < linkListLen203; linkIndex203++) {
        var linkData203 = linkList203[linkIndex203];
        output += '<li><a href="' + soy.$$escapeHtml(linkData203.href) + '">' + soy.$$escapeHtml(linkData203.text) + '</a></li>';
      }
    } else {
      output += '&nsbp;';
    }
  } else {
    output += '<li><a rel="external" href="http://news.gc.ca/web/dsptch-dstrbr-en.do?mthd=advSrch&#38;crtr.dpt1D=420&#38;crtr.thm1D=&#38;crtr.tp1D=1&#38;crtr.sj1D=&#38;crtr.lc1D=&#38;crtr.aud1D=&#38;crtr.kw=&#38;crtr.dyStrtVl=1&#38;crtr.mnthStrtVl=1&#38;crtr.yrStrtVl=2002&#38;crtr.dyndVl=1&#38;crtr.mnthndVl=1&#38;crtr.yrndVl=3000">News releases</a></li><li><a rel="external" href="http://news.gc.ca/web/dsptch-dstrbr-en.do?mthd=advSrch&#38;crtr.dpt1D=420&#38;crtr.thm1D=&#38;crtr.tp1D=3&#38;crtr.sj1D=&#38;crtr.lc1D=&#38;crtr.aud1D=&#38;crtr.kw=&#38;crtr.dyStrtVl=1&#38;crtr.mnthStrtVl=1&#38;crtr.yrStrtVl=2002&#38;crtr.dyndVl=1&#38;crtr.mnthndVl=1&#38;crtr.yrndVl=3000">Media advisories</a></li><li><a rel="external" href="http://news.gc.ca/web/dsptch-dstrbr-en.do?mthd=advSrch&#38;crtr.dpt1D=420&#38;crtr.thm1D=&#38;crtr.tp1D=970&#38;crtr.sj1D=&#38;crtr.lc1D=&#38;crtr.aud1D=&#38;crtr.kw=&#38;crtr.dyStrtVl=1&#38;crtr.mnthStrtVl=1&#38;crtr.yrStrtVl=2002&#38;crtr.dyndVl=1&#38;crtr.mnthndVl=1&#38;crtr.yrndVl=3000">Speeches</a></li><li><a href="/eng/stories/index.shtml">Success Stories</a></li>';
  }
  output += '</ul></section><section class="col-sm-3"><h3>Stay connected</h3><ul class="list-unstyled"><li><a rel="external" href="http://www.youtube.com/hrsdcanada">YouTube</a></li><li><a rel="external" href="https://twitter.com/Jobs_Emplois">Twitter: @Jobs_Emplois</a></li><li><a rel="external" href="https://twitter.com/SocDevSoc">Twitter: @SocDevSoc</a></li><li><a rel="external" href="https://www.flickr.com/photos/121918683@N07/sets/">Flickr: ESDC_EDSC</a></li><li><a rel="external" href="http://www.workingincanada.gc.ca/widget-eng.do">Job Bank Widget</a></li><li><a href="http://www.esdc.gc.ca/eng/consultations/index.shtml">Consultation and Engagement</a></li><li><a href="http://www.esdc.gc.ca/eng/connected/commenting.shtml">Commenting Standards</a></li></ul></section></div></nav></div><div id="gc-info"><div class="container"><nav role="navigation"><h2>Government of Canada footer</h2><ul class="list-inline"><li><a href="http://healthycanadians.gc.ca"><span>Health</span></a></li><li><a href="http://travel.gc.ca"><span>Travel</span></a></li><li><a href="http://www.servicecanada.gc.ca/eng/home.shtml"><span>Service Canada</span></a></li><li><a href="http://www.jobbank.gc.ca"><span>Jobs</span></a></li><li><a href="http://actionplan.gc.ca/en"><span>Economy</span></a></li><li id="canada-ca"><a href="http://www.canada.ca/en/index.html">Canada.ca</a></li></ul></nav></div></div></footer>';
  return output;
};


wet.builder.refFooter = function(opt_data, opt_ignored) {
  opt_data = opt_data || {};
  return '<!--[if gte IE 9 | !IE ]><!--><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"><\/script><script src="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/js/wet-boew.min.js"><\/script><!--<![endif]--><!--[if lt IE 9]><script src="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/js/ie8-wet-boew2.min.js"><\/script><![endif]--><script src="' + ((opt_data.templateDomain != null) ? soy.$$escapeHtml(opt_data.templateDomain) : (opt_data.cdnEnv == 'qat') ? 'https://cdn-canada.services.gc.qat' : (opt_data.cdnEnv == 'nonprod') ? 'https://s2tst-cdn-canada.sade-edap.prv' : 'https://ssl-templates.services.gc.ca') + '/app/cls/WET/utweb/v4_0_16/js/theme.min.js"><\/script>';
};
