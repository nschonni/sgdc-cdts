SET version=v4_0_32
java -jar SoyToJsSrcCompiler.jar --outputPathFormat nomin.js --srcs ..\app\cls\WET\gcintranet\%version%\cdts\uncompiled\wet-en.soy,..\app\cls\WET\gcintranet\%version%\cdts\uncompiled\serverPage.soy
rem java -jar minify.jar --js nomin.js --js_output_file ..\app\cls\WET\gcintranet\%version%\cdts\compiled\wet-en.js
java -jar minify.jar --js nomin.js --js ..\app\cls\WET\global\%version%\js\*.js --js_output_file ..\app\cls\WET\gcintranet\%version%\cdts\compiled\wet-en.js

java -jar SoyToJsSrcCompiler.jar --outputPathFormat nomin.js --srcs ..\app\cls\WET\gcintranet\%version%\cdts\uncompiled\wet-fr.soy,..\app\cls\WET\gcintranet\%version%\cdts\uncompiled\serverPage.soy
rem java -jar minify.jar --js nomin.js --js_output_file ..\app\cls\WET\gcintranet\%version%\cdts\compiled\wet-fr.js
java -jar minify.jar --js nomin.js --js ..\app\cls\WET\global\%version%\js\*.js --js_output_file ..\app\cls\WET\gcintranet\%version%\cdts\compiled\wet-fr.js

rem java -jar minify.jar --js ..\app\cls\WET\global\%version%\js\*.js --js_output_file ..\app\cls\WET\gcweb\%version%\cdts\compiled\sgdc-cdts.min.js
pause