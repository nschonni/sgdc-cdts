#CDTS Stats update

## How to generate the CDTS report on Akamai

https://control.akamai.com

### Part 1

1. Click on **Monitor** on the top menu bar
2. Click on **User traffic**
3. Click on **Change** (next to the question mark)
4. Check the **Select All** next to the *Traffic Segments:* text
5. Click on **custom date range** and select your range
6. Click on **OK**

### Part 2
1. Scroll to the **Top Url** section 
2. Click on the **dropdown** next to filter
3. Select **volume details**
4. Click on **filter**
5. Enter cdts and click **filter**
6. Click on the **export button** on the left of the filter option
7. Export to **CSV**
8. Save it in the `/stats/2019` folder

* Sum column D (OK Hits)
* Copy to the Akamai Hits column in the CDTS Stats workbook (cdtsstats.xlsx)

## Updating the SSL stats

http://web-stats.prv/Index-e.asp

* Navigate to **DMZ > ssl_templates.gc.ca > Year > Monthly-Mensuel > Month**
* Copy the values from **Total Hits - Total des Hits** to the SSL Template Hits in the CDTS Stats workbook (cdtsstats.xlsx)

## Updating the CDTS Stats workbook (cdtsstats.xlsx) 

* Update adverage formula
